/**
 *	Evaluates if a given object has a given property
 *	Function hasOwnProperty is forbidden
 *
 *	@function	hasProperty
 *	@param		object obj
  *	@param		string propertyName
 *	@return		boolean
 */
function hasProperty(obj, propertyName) {
	
}