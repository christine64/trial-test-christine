/*
 *    @function    {Object} Person
 *    @param        string name
 */
function Person(value) {
    let updatedName;
    let personProfession;

    let personConstructor = function(value) {
        return updatedName;
    }
    
    personConstructor.setName = function(newName) {
        updatedName = newName;
    };

    personConstructor.getName = function() {
        return updatedName;
    };

    personConstructor.profession = function(profession) {
        if (profession) {
            personProfession = profession
        } else {
            personProfession;
        }
        return personProfession;
    };

    personConstructor.introduce = function() {
      console.log('My name is ' + updatedName + ' I work as a ' + personProfession);
    };

    return personConstructor;
}

var Mike = Person('John Doe');
var Bob = Person('John Doe');
var Worker = Person('John Doe');

/*
 * Directions Part 1:
 *     - Update the Person function so the below statements don't generate any errors
 */
Mike.setName('Mike');    // name setter
Mike.getName();         // name getter


/*
 * Directions Part 2:
 *     - Update the Person function so the below statements don't generate any errors
 */
Bob.setName('Bob');    // name setter
Bob.getName();          // name getter


/*
 * Directions part 3:
 * - Update the Person function so the below statements don't generate any errors
 *
 */
Worker.setName('Kevin');            // name setter
Worker.profession('Programmer');    // profession setter
Worker.profession();                // profession getter

Worker.introduce();                    // Calling this method, the Person will introduce themselves with their name and profession. 