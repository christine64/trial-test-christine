/*
 *	Directions
 *	Write a Javascript program that will return an array populated with the following values:
 *		- the current date formatted: DD-MM-YYYY
 *		- the number of days before next christmas
 *	Note
 *	- Native Javascript only
 *
 *	@function	app
 *
 */
function app() {
  var arrayOfDates = [];
  var currentDate = new Date();
  christmas = new Date("December 25, 2018");
  timeDifference = (christmas.getTime() - currentDate.getTime());
  milli = 24 * 60 * 60 * 1000;
  daysLeft = timeDifference / milli;
  daysUntilChristmas = Math.floor(daysLeft);
  arrayOfDates.push(currentDate.toLocaleDateString(), daysUntilChristmas);

  return arrayOfDates;
}